let amigos = ["Luis","Carlos","Jefferson","Jorge","Yimi"];

// console.log(amigos)

// amigos.push("Fenix");

// let date = amigos.slice(0,2);

// console.log(amigos)
// console.log(date)

for (let i=0; i<amigos.length; i++){
    console.log(amigos[i]);
}

//foreach necesita como parametro una funcion
//la funcion necesita parametros

/*
amigos.forEach(function(i){
    console.log(i);
})

amigos.forEach(i =>{
    console.log(i);
})
*/

//a una sola linea
console.log("**********nueva forma")
amigos.forEach(i => console.log(i));
//Foreach no me devuelve nada

//Ahora con .map 
//map devuelve los datos
let dato = amigos.map(i => `Hola ${i}`);
console.log(dato)


let numeros = [10, 20, 4, 2, 40, 33, 22, 11];
let newArray = [];
// numeros.map(num =>{
//     if(num>20){
//         console.log(num)
//         newArray.push(num)
//     }
// })

let newDato = numeros.filter(num => num > 20)

console.log(newDato)
