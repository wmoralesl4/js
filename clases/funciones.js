function saludar(nombre){
    if (typeof nombre === "string"){
        console.log(`Hola ${nombre}`)
    }else{
        console.log(`Ingrese un dato correcto`)    
    }
}

function obtenerNombre(nombre, apellido){
    let completo=`${nombre} ${apellido}`

    return completo
}
saludar(123);


console.log(obtenerNombre("wilson", "alberto"));


// funcion anonima (SIN NOMBRE)
let suma = function(a,b){
    return a+b;
}
console.log(suma(10,20));

//Otra forma mas viejita

(function(a,b,c){
    console.log(a+b+c);
}(10,4,3))

//Funciones como constantes
const saludar2 = function(nombre){
    console.log(`hola otra vezzzz ${nombre}`)
    return "JJA"
}

console.log(saludar2("Mario"))


//Scope -- Alcance

let nickname = "noob"

function saludando(){
    console.log(nickname)
}

saludando()


//FUNCIONES FLECHA **********************
// =>
const sumanding = function(a,b){
    return a+b;
}
const restanding = function(a,b){
    return a-b;
}
console.log(sumanding(10,5) + " " +restanding(20,4))

//Sin la palabra fuction
//retornando lo mismo
const sumanding2 = (a,b) => a+b;
const restanding2 = (a,b) => a-b;
console.log(sumanding2(10,5) + " " +restanding2(20,4))

//Cuando solo recibo un PARAMETRO: Debo borrar parentesis
const mensaje = nombre => `Hola ${nombre}`
console.log(mensaje("AREVALo"));

//Con if 
const mensaje2 = nombre =>{
    if (typeof nombre === "string"){
        console.log(`Hola ${nombre}`);
    }else{
        console.error("Dato equivocado")
    }
}

mensaje2(222)
