let numeros = [10, 20, 4, 2, 40, 33, 22, 11];

//La funcion dato solo devuelve el primer dato que encuentre

let newDato = numeros.find(num => num > 20)
console.log(newDato)

//include, te dice si lo incluye o no

newDato = numeros.includes(4)
console.log(newDato)

//some si dice si alguno cumple la condicion o no
newDato = numeros.some(num => num < 0)
console.log(newDato)

//every si todos cumplen la condicion

newDato = numeros.some(num => typeof num === "number")
console.log(newDato)