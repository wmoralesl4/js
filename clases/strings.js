let texto = "Wilson Alberto Morales Lazaro"

//Slice devuelve(parte) desde la posicion a hasta la b

let dato = texto.slice(10,20)
console.log(dato)

//Split va a dividir el texto

dato = texto.split(" ")
console.log(dato)


dato = texto.toLocaleUpperCase()
console.log(dato)

//************ OBJETOS  */


let alumno = {
    nombre: "Wilson",
    edad: 21,
    suscriptor: false,
    ciudad: "guatemala"
}

console.log(alumno.edad)
console.log(alumno["ciudad"])

//Obtener todos los datos
let valores = Object.values(alumno)
console.log(valores)

//Obtener todos los atributos
valores = Object.keys(alumno)
console.log(valores)
