const usuario = {
    nombre: "Wilson Morales",
    edad: 21,
    deuda: 0
}

let pedido = []
let costoPedido = 0;

const mostrarMenu = () => {
    console.log(`codigo - producto - precio`)
    productos.forEach(producto => console.log(`${producto.codigo} - ${producto.nombre} - Q${producto.costo}`))
    
}


const pedirProducto = cod => {

    if(!cod) return "Ingrese un codigo valido"
    
    const productoEncontrado = productos.find(producto => producto.codigo === cod)
    if(!productoEncontrado) return "El producto no existe"

    pedido.push(productoEncontrado)
    console.log("Producto agregado con exito")
    return verPedido()
}

const verPedido = () => pedido

const calcularCosto = () => {
    costoPedido = 0;
    for (producto of pedido){
        costoPedido += producto.costo
    }
    return costoPedido;
}

const finalizarPedido = () => {
    calcularCosto()
    usuario.deuda = costoPedido

    pedido = []
    costoPedido = 0
    return `${usuario.nombre} debes pagar Q${usuario.deuda}.00`
}

const pagarPedido = cant =>{
    if (cant < usuario.deuda){
        return `No te alcanza para pagar tu deuda`
    }else if(cant === usuario.deuda){
        usuario.deuda = 0;
        return `Tu pedido ha sido cancelado`
    }else {
        let cambio = cant - usuario.deuda
        usuario.deuda = 0;
        return `Tu pedido ha sido pagado y tu cambio es ${cambio}`
    }
}